jQuery UI Fancy Selector Widget
==============================

This is a selector widget that replaces a standard select box with a CSS formatted one. The original concept and much of the code is derived from work of [Olav Andreas Lindekleiv][1] and [Design with PC][5]. Code has been assembled to its current state by [Steve Scheidecker][4]. It is available on [BitBucket][2]. You can also try out a live demo [here][3].



Usage
-----

To use the widget, simply create a select element, and enable it. Use the "data-style" attribute to define CSS properties for the select item text.  Use the "data-image" attribute to define a image/icon for the select item.

      	<select style="width:120px;" class="ddFancy" id="favorite_color">
	      <option data-style="color:#ff0000" value="red" selected>Red</option>
	      <option data-style="color:#0000FF" value="blue">Blue</option>
	      <option data-style="color:#00ff00" value="green">Green</option>
	      <option data-style="color:#FFFF00" value="yellow">Yellow</option>
	    </select>

		<select style="width:240px" class="ddFancy" id="favorite_network">
	      <option data-image="twitter-icon-32.png" value="twitter" selected>Twitter</option>
	      <option data-image="facebook-icon-32.png" value="facebook">Facebook</option>
	      <option data-image="linkedin-icon-32.png" value="linkedin">LinkedIN</option>
	      <option data-image="foursquare-icon-32.png" value="foursquare">FourSquare</option>
	    </select>


To enable it, do:

      $('select.ddFancy').ddFancy({
        options: {
          inSpeed: 250,
          outSpeed: "slow",
          iconWidth: 32,
          iconHeight: 32,
          height:25
        },
        itemChange: function(e, ui) {
          alert("The font is set to "+ui.item+" (was "+ui.oldItem+" before)");
        }
        closeOnSelect: true // Closes the select box when a selection is made
      });

The options are optional. Default values are 500 ms for inSpeed and 250 ms for outSpeed.


License
-------

The jQuery UI Fancy selector widget is available under the BSD License. Please read it (see the LICENSE file). It's only a few lines.


  [1]: http://lindekleiv.com/
  [2]: https://bitbucket.org/stevezilla/fancy-drop-down
  [3]: http://stevezilla.bitbucket.org/fancy-drop-down/
  [4]: http://stevezilla.com
  [5]: http://www.designwithpc.com
